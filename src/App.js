import React from 'react';
import { Provider } from 'react-redux';
import './App.css';
import Chat from './components/chat/Chat';
import { store } from './store/store';

function App() {
  return (
    <Provider store={store}>
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
    </Provider>
    
  );
}

export default App;
