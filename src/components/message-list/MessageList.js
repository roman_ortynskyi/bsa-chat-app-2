import React, { useContext } from 'react';
import { ChatContext } from '../chat/Chat';
import Message from '../message/Message';
import OwnMessage from '../own-message/OwnMessage';

import './MessageList.css';

function MessageList({ messages, onDeleteMessage, onEditMessage, onLikeMessage }) {
    console.log(messages);

    const { messageListRef } = useContext(ChatContext);

    const messageListJSX = messages.map(message => {
        const messageComponent = message.isMine ? 
            <OwnMessage 
                key={message.id} 
                {...message} 
                onDelete={onDeleteMessage} 
                onEdit={onEditMessage} 
            /> 
            : <Message 
                key={message.id} 
                {...message} 
                onLike={onLikeMessage}
            />;
        return messageComponent;
    });

    return (
        <div className="message-list" ref={messageListRef}>
            {messageListJSX}
        </div>
    );
}

export default MessageList;