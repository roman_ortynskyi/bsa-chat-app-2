import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { chatActions } from '../../store/chat/actions';

import './EditMessageModal.css';

function EditMessageModal() {
    const dispatch = useDispatch();
    const { editingMessageId, messages } = useSelector(state => state.chat);
    
    const messageIndex = messages.findIndex(el => el.id === editingMessageId);

    const message = messages[messageIndex];

    // console.log(messages)
    // console.log(editMessageId);
    // 

    const [text, setText] = useState(message.text);

    const saveChanges = () => {
        const updatedMessage = {
            ...message,
            text
        };

        const updatedMessages = [
            ...messages.slice(0, messageIndex),
            updatedMessage,
            ...messages.slice(messageIndex + 1)
        ];
        
        dispatch(chatActions.setMessages(updatedMessages));
        dispatch(chatActions.setEditingMessageId(null));
        dispatch(chatActions.setEditModal(false));
    };

    const hideModal = () => {
        dispatch(chatActions.setEditingMessageId(null));
        dispatch(chatActions.setEditModal(false));
    }

    return (
        <div id="demo-modal" className="edit-message-modal">
            <div className="modal__content">
                <h1>Edit Message</h1>

            <textarea 
                className="edit-message-input"
                value={text}
                onChange={(e) => setText(e.target.value)}
            ></textarea>

                <div className="modal__footer">
                    <button 
                        className="edit-message-button"
                        onClick={saveChanges}
                    >OK</button>
                    <button 
                        className="edit-message-close"
                        onClick={hideModal}
                    >Cancel</button>
                </div>
            </div>
        </div>
    );
};

export default EditMessageModal;