import React, { 
    useState, 
    useEffect, 
    createContext, 
    useRef
} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import ChatHeader from '../header/Header';
import MessageList from '../message-list/MessageList';
import MessageInput from '../message-input/MessageInput';

import './Chat.css';
import { chatActions } from '../../store/chat/actions';
import Spinner from '../Spinner/Spinner';
import EditMessageModal from '../EditMessageModal/EditMessageModal';

export const ChatContext = createContext();

function Chat({ url }) {
    const { messages, preloader, editModal } = useSelector(state => state.chat);
    const dispatch = useDispatch();

    const messageListRef = useRef();

    const contextValue = {
        messageListRef
    };

    useEffect(() => {
        (async () => {
            dispatch(chatActions.setPreloader(true));

            const response = await fetch(url, {
                method: 'GET'
            });
            const data = await response.json();

            const messages = data.map(message => ({ ...message, isLiked: false }));


            dispatch(chatActions.setMessages(messages))
            dispatch(chatActions.setPreloader(false));
        })();
    }, [url, dispatch]);

    const usersCount = new Set(messages.map(message => message.user)).size;
    const lastMessageCreatedAt = messages[messages.length - 1]?.createdAt;

    const deleteMessage = (id) => {
        const filteredMessages = messages.filter(message => message.id !== id);
        dispatch(chatActions.setMessages(filteredMessages));
    };

    const editMessage = (id) => {
        dispatch(chatActions.setEditingMessageId(id));
        dispatch(chatActions.setEditModal(true));
    };

    const likeMessage = (id) => {
        const messageIndex = messages.findIndex(el => el.id === id);

        const message = messages[messageIndex];

        const updatedMessage = {
            ...message,
            isLiked: !message.isLiked
        }

        const updatedMessages = [
            ...messages.slice(0, messageIndex),
            updatedMessage,
            ...messages.slice(messageIndex + 1)
        ];

        dispatch(chatActions.setMessages(updatedMessages));
    }

    return (
        <ChatContext.Provider value={contextValue}>
            <div className="chat">
                {preloader ? <Spinner />
                : (
                    <>
                        <ChatHeader 
                            messagesCount={messages.length}
                            usersCount={usersCount}
                            lastMessageCreatedAt={lastMessageCreatedAt}
                        />
                        <MessageList
                            messages={messages} 
                            onDeleteMessage={deleteMessage} 
                            onEditMessage={editMessage} 
                            onLikeMessage={likeMessage}
                        />
                        <MessageInput />
                        {editModal && <EditMessageModal />} 
                    </>
                )}
                
            </div>
        </ChatContext.Provider>
    );
}

export default Chat;