import React from 'react';
import moment from 'moment';

import './Header.css';

function ChatHeader({
    usersCount,
    messagesCount,
    lastMessageCreatedAt
}) {

    const lastMessageCreatedAtFormatted = moment(lastMessageCreatedAt).format('DD.MM.yyyy hh:mm');

    return (
        <div className="header">
            <span className="header-title">My chat</span>
            <span className="header-users-count">{usersCount} participants</span>
            <span className="header-messages-count">{messagesCount} messages</span>
            <span className="header-last-message-date">last message at {lastMessageCreatedAtFormatted}</span>
        </div>
    );
}

export default ChatHeader;