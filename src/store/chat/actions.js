import { types } from './types';

export const chatActions = Object.freeze({

    setMessages: (payload) => {
        return {
            type: types.SET_MESSAGES,
            payload
        };
    },

    setEditingMessageId: (payload) => {
        return {
            type: types.SET_EDITING_MESSAGE_ID,
            payload
        };
    }, 
    
    setEditModal: (payload) => {
        return {
            type: types.SET_EDIT_MODAL,
            payload
        };
    },

    setPreloader: (payload) => {
        return {
            type: types.SET_PRELOADER,
            payload
        };
    },
})