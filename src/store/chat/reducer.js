import { types } from './types';

const initialState = {
    messages: [],
    editModal: false,
    preloader: true,
    editingMessageId: null
};

export const chatReducer = (state = initialState, { type, payload }) => {
    switch(type) {
        case types.SET_MESSAGES: 
        return {
            ...state,
            messages: payload
        };

        case types.SET_EDITING_MESSAGE_ID:
        return {
            ...state,
            editingMessageId: payload
        };

        case types.SET_EDIT_MODAL:
        return {
            ...state,
            editModal: payload
        };

        case types.SET_PRELOADER:
        return {
            ...state,
            preloader: payload
        };

        default: 
            return state;
    }
}