import { combineReducers } from 'redux';

//import reducers
import { chatReducer as chat } from './chat/reducer';

export const rootReducer = combineReducers({
    // reducers
    chat,
});